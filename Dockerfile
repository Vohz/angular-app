FROM node:lts-alpine
WORKDIR /app
COPY . /app
RUN npm install \
    && npm install --save-dev intl \
    && npm install --save-dev phantomjs phantomjs-prebuilt karma-phantomjs-launcher
EXPOSE 3333
CMD [ "npm", "start"]